**Jenkins** 'upgrade' job status (pipeline type):

[![Build Status](http://www.elvislittle.site/buildStatus/icon?job=ansible%2Fupgrade)](https://www.elvislittle.site/job/ansible/job/upgrade/badge/)

---

**Jenkins** 'ping' job status (pipeline type):

[![Build Status](http://www.elvislittle.site/buildStatus/icon?job=ansible%2Fping)](https://www.elvislittle.site/job/ansible/job/ping/badge/)

---

**SonarQube** analysis:

[![Quality Gate Status](http://13.49.8.131:9000/api/project_badges/measure?project=elvislittle_ansible_AYzmNILho_94ZYu5WjBK&metric=alert_status&token=sqb_22ba2b2905359e129d40d2013ea091bb1c0b7514)](http://13.49.8.131:9000/dashboard?id=elvislittle_ansible_AYzmNILho_94ZYu5WjBK)
